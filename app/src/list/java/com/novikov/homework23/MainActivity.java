package com.novikov.homework23;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.novikov.homework23.card.Card;
import com.novikov.homework23.card.CardsAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "loggggggggggggg";

    protected RecyclerView cardRecyclerView;

    protected ImageView meImageView;
    protected ImageView familyImageView;
    protected ImageView lovelyImageView;
    protected ImageView teamImageView;
    protected ImageView friendsImageView;
    protected ImageView calendarImageView;
    protected String buildTypeRelease = "release";
    protected String buildTypeDebug = "debug";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cardRecyclerView = findViewById(R.id.cardRecyclerView);

        List<Card> dialogs = new ArrayList<>(Arrays.asList(
                new Card("Me", R.drawable.me),
                new Card("Family", R.drawable.family),
                new Card("Lovely", R.drawable.lovely),
                new Card("Team", R.drawable.team),
                new Card("Friends", R.drawable.friends),
                new Card("Calendar", R.drawable.calendar)
        ));
        final CardsAdapter adapter = new CardsAdapter(dialogs);
        adapter.setListener(this::onClickCard);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                this,
                LinearLayoutManager.VERTICAL,
                false);
        cardRecyclerView.setLayoutManager(layoutManager);
        cardRecyclerView.setAdapter(adapter);
    }

    private void onClickCard(Card card) {
        if (BuildConfig.BUILD_TYPE.equals(buildTypeDebug)) {
            switch (card.getName()) {
                case "Me":
                    Log.e(LOG_TAG, " 'Me' card pressed");
                    Toast.makeText(this, "'Me' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case "Family":
                    Log.e(LOG_TAG, " 'Family' card pressed");
                    Toast.makeText(this, "'Family' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case "Lovely":
                    Log.e(LOG_TAG, " 'Lovely' card pressed");
                    Toast.makeText(this, "'Lovely' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case "Team":
                    Log.e(LOG_TAG, " 'Team' card pressed");
                    Toast.makeText(this, "'Team' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case "Friends":
                    Log.e(LOG_TAG, " 'Friends' card pressed");
                    Toast.makeText(this, "'Friends' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case "Calendar":
                    Log.e(LOG_TAG, " 'Calendar' card pressed");
                    Toast.makeText(this, "'Calendar' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Log.e(LOG_TAG, " 'Incorrect card pressed'");
            }
        }
        if (BuildConfig.BUILD_TYPE.equals(buildTypeRelease)) {
            finish();
        }
    }
}
