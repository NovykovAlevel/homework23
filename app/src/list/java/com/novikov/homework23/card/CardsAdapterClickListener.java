package com.novikov.homework23.card;

public interface CardsAdapterClickListener {

    void onClicked(Card card);
}
