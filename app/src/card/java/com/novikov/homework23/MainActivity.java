package com.novikov.homework23;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "loggggggggggggg";

    protected String buildTypeRelease = "release";
    protected String buildTypeDebug = "debug";

    protected ImageView meImageView;
    protected ImageView familyImageView;
    protected ImageView lovelyImageView;
    protected ImageView teamImageView;
    protected ImageView friendsImageView;
    protected ImageView calendarImageView;

    protected TextView meTextView;
    protected TextView familyTextView;
    protected TextView lovelyTextView;
    protected TextView teamTextView;
    protected TextView friendsTextView;
    protected TextView calendarTextView;
    protected TextView textGrid;

    protected CardView meCardView;
    protected CardView familyCardView;
    protected CardView lovelyCardView;
    protected CardView teamCardView;
    protected CardView friendsCardView;
    protected CardView calendarCardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initImageViews();
        initTextViews();
        initCardViews();
        setListenersOnCardViews();
    }

    private void setListenersOnCardViews() {
        meCardView.setOnClickListener(this::onClickCardView);
        familyCardView.setOnClickListener(this::onClickCardView);
        lovelyCardView.setOnClickListener(this::onClickCardView);
        teamCardView.setOnClickListener(this::onClickCardView);
        friendsCardView.setOnClickListener(this::onClickCardView);
        calendarCardView.setOnClickListener(this::onClickCardView);
    }

    private void initCardViews() {
        meCardView = findViewById(R.id.meCardView);
        familyCardView = findViewById(R.id.familyCardView);
        lovelyCardView = findViewById(R.id.lovelyCardView);
        teamCardView = findViewById(R.id.teamCardView);
        friendsCardView = findViewById(R.id.friendsCardView);
        calendarCardView = findViewById(R.id.calendarCardView);
    }

    private void initTextViews() {
        meTextView = findViewById(R.id.meTextView);
        familyTextView = findViewById(R.id.familyTextView);
        lovelyTextView = findViewById(R.id.lovelyTextView);
        teamTextView = findViewById(R.id.teamTextView);
        friendsTextView = findViewById(R.id.friendsTextView);
        calendarTextView = findViewById(R.id.calendarTextView);
        textGrid = findViewById(R.id.textGrid);
    }

    private void initImageViews() {
        meImageView = findViewById(R.id.meImageView);
        familyImageView = findViewById(R.id.familyImageView);
        lovelyImageView = findViewById(R.id.lovelyImageView);
        teamImageView = findViewById(R.id.teamImageView);
        friendsImageView = findViewById(R.id.friendsImageView);
        calendarImageView = findViewById(R.id.calendarImageView);
    }

    private void onClickCardView(View v) {
        if (BuildConfig.BUILD_TYPE.equals(buildTypeDebug)) {
            switch (v.getId()) {
                case R.id.meCardView:
                    Log.e(LOG_TAG, " 'Me' card pressed");
                    Toast.makeText(this, "'Me' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.familyCardView:
                    Log.e(LOG_TAG, " 'Family' card pressed");
                    Toast.makeText(this, "'Family' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.lovelyCardView:
                    Log.e(LOG_TAG, " 'Lovely' card pressed");
                    Toast.makeText(this, "'Lovely' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.teamCardView:
                    Log.e(LOG_TAG, " 'Team' card pressed");
                    Toast.makeText(this, "'Team' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.friendsCardView:
                    Log.e(LOG_TAG, " 'Friends' card pressed");
                    Toast.makeText(this, "'Friends' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.calendarCardView:
                    Log.e(LOG_TAG, " 'Calendar' card pressed");
                    Toast.makeText(this, "'Calendar' card pressed", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Log.e(LOG_TAG, "Incorrect card pressed");
            }
        }
        if (BuildConfig.BUILD_TYPE.equals(buildTypeRelease)) {
            finish();
        }
    }
}
